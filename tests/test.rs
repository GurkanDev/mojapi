use mojapi::Player;

#[test]
fn test_make_player_name() {
    let player = Player::new("gurkz");

    assert_eq!(
        player,
        Player {
            name: Some("gurkz".to_string()),
            uuid: None
        }
    )
}

#[test]
fn test_make_player_uuid() {
    let player = Player::new("cd95c718-0580-4b57-810c-52fa2b143051");

    assert_eq!(
        player,
        Player {
            name: None,
            uuid: Some("cd95c71805804b57810c52fa2b143051".to_string())
        }
    )
}

#[test]
fn test_get_player_name() {
    let player = Player::new("cd95c718-0580-4b57-810c-52fa2b143051");

    assert_eq!(player.name().unwrap(), "gurkz".to_string())
}
