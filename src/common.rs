use std::time::Duration;

use ureq::Agent;
use ureq::AgentBuilder;

// default Agent for all requests
pub fn ureq_agent() -> Agent {
    AgentBuilder::new()
        .timeout(Duration::from_secs(5))
        .user_agent("mojapi/0.0.0")
        .build()
}
