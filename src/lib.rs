use tinyjson::JsonValue;

mod common;

#[derive(Debug, Clone, Copy)]
pub enum MojangError {
    NoNameOrUUID,

    RequestError,
    ParseError,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Player {
    pub name: Option<String>,
    pub uuid: Option<String>,
}

impl Player {
    pub fn new<T>(name_uuid: T) -> Player
    where
        T: std::fmt::Display,
    {
        // if length is less than 16 characters, then input must be a name
        if name_uuid.to_string().len() < 16 {
            return Player {
                name: Some(name_uuid.to_string()),
                ..Player::default()
            };
        }
        // if length is 16 or more characters, then input must be a uuid
        // or some other nonsense.

        Player {
            uuid: Some(name_uuid.to_string().to_lowercase().replace("-", "")),
            ..Player::default()
        }
    }

    pub fn name(&self) -> Result<String, MojangError> {
        // if we already have a name, return it
        if self.name.is_some() {
            return Ok(self.name.clone().unwrap());
        }

        // if we don't have a name, then get it from the Mojang API
        if self.uuid.is_some() {
            let agent = common::ureq_agent();
            match agent
                .get(&format!(
                    "https://sessionserver.mojang.com/session/minecraft/profile/{}",
                    self.uuid.clone().unwrap()
                ))
                .call()
            {
                Ok(i) => match &i.into_string().unwrap().parse::<JsonValue>().unwrap()["name"] {
                    JsonValue::String(i) => return Ok(i.to_string()),
                    _ => return Err(MojangError::ParseError),
                },

                Err(_) => return Err(MojangError::RequestError),
            };
        }

        Err(MojangError::NoNameOrUUID)
    }

    pub fn uuid(&self) -> Result<String, MojangError> {
        unimplemented!()
    }
}

impl Default for Player {
    fn default() -> Player {
        Player {
            name: None,
            uuid: None,
        }
    }
}
